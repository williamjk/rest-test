package avenuecode.rest_test;


import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

/** http://jsoup.org
 *  Example program to list links from a URL.
 */
public class ListLinks {
    public static void main(String[] args) throws IOException {
       // Validate.isTrue(args.length == 1, "usage: supply url to fetch");
        String url = "http://www.fanatics.com/search/fanatics";//args[0];
        print("Fetching %s...", url);

        Document doc = Jsoup.connect(url).get();
        //Elements links = doc.select("a[href]");
        Elements media = doc.select("a");
        Elements imports = doc.select("meta");

        print("\nMedia: (%d)", media.size());
        for (Element src : media) {
            if (src.tagName().equals("a") &&  !src.attr("id").isEmpty() && !src.attr("title").isEmpty() && src.attr("title").length() > "Don't see your team? View all 500+ NCAA teams >>".length()){
            	//System.out.println(src);
            	 print("  %s , %s ",
                        src.attr("id"), src.attr("title")
            	     ); 
            }
           }

       print("\nImports: (%d)", imports.size());
        for (Element link : imports) {
        	//if (link.attr("itemprop").toString().equalsIgnoreCase("description")) //get just description
            print("  %s , %s", link.attr("itemprop"), link.attr("content"));
        }
       
    }

    private static void print(String msg, Object... args) {
        System.out.println(String.format(msg, args));
    }

}
