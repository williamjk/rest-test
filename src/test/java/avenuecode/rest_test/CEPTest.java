package avenuecode.rest_test;

import static com.jayway.restassured.RestAssured.get;
import static org.hamcrest.Matchers.equalTo;

import org.testng.annotations.Test;

public class CEPTest {

	String false_url = "http://correiosapi.apphb.com/cep/9496510";
	String true_url = "http://correiosapi.apphb.com/cep/94965100";
	String base_url = "http://correiosapi.apphb.com/cep/";

	@Test (description = "Test Url false")
	public void false_test() {

		String returnesAdressNotExists = get(false_url).asString();
		System.out.println(returnesAdressNotExists);

		get(false_url).then().body("message", equalTo("Endereço não encontrado!"));
	}

	@Test (description = "Test url valid")
	public void true_test() {

		String returnesAdressExists = get(true_url).asString();
		System.out.println(returnesAdressExists);

		get(true_url).then().assertThat().body("cep", equalTo("94965100")).and().body("tipoDeLogradouro", equalTo("Rua")).and().body("logradouro", equalTo("Dinamarca")).and().body("bairro", equalTo("Parque Marechal Rondon")).and().body("cidade", equalTo("Cachoeirinha")).and().body("estado", equalTo("RS"));
	}
	
	@Test (description = "Assert city of cep", dataProvider = "CEP_assertCity", dataProviderClass = DataTest.class)
	public void assert_city_of_cep_test(int cep, String city ) {

		String returnesAdressExists = get(base_url+cep).asString();
		System.out.println(returnesAdressExists);

		get(base_url+cep).then().assertThat().body("cidade", equalTo(city));
	}

}

/*
 * EXAMPLE { "lotto":{ "lottoId":5, "winning-numbers":[2,45,34,23,7,5,3],
 * "winners":[{ "winnerId":23, "numbers":[2,45,34,23,3,5] },{ "winnerId":54,
 * "numbers":[52,3,12,11,18,22] }] } }
 * 
 * // CORREIOS
 * {"cep":"94965100","tipoDeLogradouro":"Rua","logradouro":"Dinamarca","bairro":
 * "Parque Marechal Rondon","cidade":"Cachoeirinha","estado":"RS"}
 */

/*
 * 	J.CAR PEÇAS
 *	41 3349-3333	
 *	vendasjcar@gmail.com
 * 
 * 963 - hightech - elton
 */

