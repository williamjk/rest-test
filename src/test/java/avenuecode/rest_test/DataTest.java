package avenuecode.rest_test;

import org.testng.annotations.DataProvider;

public class DataTest {

	@DataProvider(name = "CEP_assertCity", parallel = false)
	public static Object[][] createData1() {
		return new Object[][] { { 94965100, "Cachoeirinha" }, { 30130151, "Belo Horizonte" }, };
	}

}
